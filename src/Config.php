<?php

namespace Tool;

class Config
{
    public static $data ;

    /**
     * 获取参数 用户适配各种框架
     * @param $name
     * @param $default
     * @return array|false|mixed|string|null
     */
//    public static function get_old($name,$default = null)
//    {
//        if (function_exists('env')){ // thinkphp laravel
//            return env($name,$default);
//        }elseif (function_exists('getEnv')){ // yaf
//            return getEnv($name,$default);
//        }else{
//            return $default;
//        }
//    }


    /**
     * 获取配置参数
     * @param $name
     * @param $default
     * @return mixed|null
     */
    public static function get($name,$default = null)
    {
        $path = dirname(__FILE__).'/../../../../.env';
        if (file_exists($path)){
            $env = parse_ini_file($path, true, INI_SCANNER_RAW) ?: [];
            $env = array_change_key_case($env, CASE_UPPER);
            foreach ($env as $key => $val) {
                if (is_array($val)) {
                    foreach ($val as $k => $v) {
                        self::$data[$key . '_' . strtoupper($k)] = $v;
                    }
                } else {
                    self::$data[$key] = $val;
                }
            }
//            $key = str_replace('.','_',$name);
//            if (array_key_exists($key,self::$data)){
//                return self::$data[$key];
//            }else{
//                return $default;
//            }
        }

        $key = strtoupper(str_replace('.','_',$name));
        return isset(self::$data[$key])?self::$data[$key]:$default;
    }
}