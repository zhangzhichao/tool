<?php

namespace Tool;

use Tool\Config;
trait Http
{
    /**
     * @param array $option = [
     *              <p>'url'           => '', // 请求路径 </p>
     *              <p>'data'          => [],</p>
     *              <p>'type'          => 'GET',</p>
     *              <p>'data_type'     => 'json',</p>
     *              <p>'is_ssl'        => false,</p>
     *              <p>'ssl_path'      => '',</p>
     *              ]
     * @return mixed
     */
    public static function http(Array $option = [])
    {
        $array = [
            'url'           => '',
            'data'          => [],
            'type'          => 'GET',
            'data_type'     => 'json',
            'is_ssl'        => false,
            'ssl_path'      => '',
        ];

        $data_type_option = [
            'GET',
            'POST',
            'PUT',
            'DELETE',
            'HEAD',
            'OPTIONS',
            'TRACE',
            'CONNECT',
        ];

        $array = array_merge($array,$option);

        list(
            'url'   => $url,
            'data'  => $data,
            'type'  => $type,
            'data_type' => $data_type,
            'is_ssl'    => $is_ssl,
            'ssl_path'  => $ssl_path
            ) = $array;

        if (empty($url)) throw new \Exception("Value url is empty");

        if (!in_array(strtoupper($type),$data_type_option)) throw new \Exception('type most in [GET|POST|PUT|DELETE|HEAD|OPTIONS|TRACE|CONNECT]');

        $ch = curl_init();

        if ($is_ssl){
            if (empty($ssl_path))  throw new \Exception("Value url is empty");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
            curl_setopt($ch, CURLOPT_CAINFO, $ssl_path);
        }else{
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        if ( strtolower($type) == 'get' ) {
            $i = 0;
            foreach ($data as $k=>$v){
                $l = $i?'&':'?';
                if (strpos($v , '=')){
                    $url .= "$l$v";
                }else{
                    $url .= "$l$k=$v";
                }
                $i++;
            }
        }

        switch (strtolower($data_type)){
            case  'json' :
                $outFun = 'json_decode';
                $headerArray = array("Content-type:application/json;","Accept:application/json");
                break;
            default :
                $outFun = '';
                $headerArray = [];

        }

        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,strtoupper($data_type));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headerArray);
        $output = curl_exec($ch);
        curl_close($ch);
        if ($outFun == 'json_decode'){
            return json_decode($output,true);
        }else{
            return $output;
        }
    }

}