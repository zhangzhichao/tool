# tool类
> PHP常用工具类集合

## 安装方法
``` 
composer require zhangzc/tool 
```

## http 方法
> 集成 curl 的各个方法请求 （暂时只支持 GET POST）
### 使用方法

> 引用工具类
`use Tool/Tool;`

> 调用方法
```phpregexp
 $result = Tool::http([
    'url'       => 'https://www.eg.com/',
    'data'      => ['a'=>'a','b'=>'b','c=c'],
    'type'      => 'GET',
    'data_type' => 'json',
 ]);
```